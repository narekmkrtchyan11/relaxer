const container = document.getElementById("container");
let p = document.getElementById("text")

function circleText  () {
    p.innerText = "Breathe in";
    setTimeout(() => {
      p.innerText = "Hold";
      setTimeout(() => {
        p.innerText = "Breath Out";
      }, 1500);
    }, 3000);
  };
  
  function circle () {
    container.classList.remove("shrink");
    container.classList.add("grow");
    setTimeout(() => {
      setTimeout(() => {
        container.classList.remove("grow");
        container.classList.add("shrink");
      }, 3000);
    }, 1500);
    circleText();
  };
  circle();
  setInterval(circle, 7500);

